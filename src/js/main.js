'use strict';

import Vue from 'vue';
import VueRouter from 'vue-router';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import Home from 'home.vue';
import Header from 'header.vue';
import WallArtProducts from 'wallArtCards.vue';
import PointCovers from 'pointCoverCards.vue';
import Contact from 'contact.vue';
import Description from 'descriptionHandler.vue';
import '../v-time.css';

// 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter and then call `Vue.use(VueRouter)`.
Vue.use(VueRouter);

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  {
    name: 'home',
    path: '/',
    components: {
      header: Header,
      content: Home
    }
  },
  {
    name: 'wallArtCards',
    path: '/wall-art',
    components: {
      header: Header,
      content: WallArtProducts
    }
  },
  {
    path: '/wall-art/:pathName',
    name: 'wallArt',
    props: {
      header: false,
      content: true
    },
    components: {
      header: Header,
      content: Description
    }
  },
  {
    name: 'pointCoverCards',
    path: '/point-covers',
    components: {
      header: Header,
      content: PointCovers
    }
  },
  {
    name: 'contact',
    path: '/contact',
    components: {
      header: Header,
      content: Contact
    }
  }
];

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
});

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
const app = new Vue({
  router
}).$mount('#app');

// Now the app has started!
